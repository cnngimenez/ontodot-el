# Ontodot Emacs Interface
Load the current Turtle, N3 or RDF file and create images about the graph.

![ontodot-el video](http://crowd.fi.uncoma.edu.ar/wiki/lib/exe/fetch.php/christian:software:ontodot-el.gif)

[See video in Peertube](https://peertube.cipherbliss.com/videos/watch/e4ce063c-888e-43ed-af3a-a974f77ddfd6)

# Requirements

- [SWI Prolog ](https://www.swi-prolog.org/)
- ontodot library. Use `pack_install(ontodot)` from swipl ([Gitlab repository](https://gitlab.com/cnngimenez/ontodot) [Bitbucket repository](https://bitbucket.org/cnngimenez/ontodot/)).

# Useful Interactive Commands

- `ontodot-load-file`
- `ontodot-show-node`
- `ontodot-load-current-prefixes`
- `ontodot-load-current-file`
- `ontodot-show-current-node`
- `ontodot-quit-subprocess`

# License
![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

This work is under the GPL v3 License.

See COPYING.txt file or [the GNU GPL License page](https://www.gnu.org/licenses/gpl.html).
