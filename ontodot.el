;;; ontodot.el --- 

;; Copyright 2020 cnngimenez
;;
;; Author: cnngimenez
;; Version: $Id: ontodot.el,v 0.0 2020/03/21 01:42:46  Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'ontodot)

;;; Code:

(provide 'ontodot)
(eval-when-compile
  (require 'cl))

(defconst ontodot-temp-png-file "/tmp/ontodot-graph.png"
  "The temporal file where to store the generated graph.")

(defvar ontodot-subprocess nil
  "The SWI subprocess. nil if not created.")

                                        ; Subprocess management
(defvar ontodot-filter-hook nil
  "This hook is called when the filter found an SWI prompt. This means
 that the output or the task given to the subprocess is done.")

(defvar ontodot-subprocess-output ""
  "Saved chunks obtained from the subprocess output.
The process output is received as 400 bytes chunks. It must be saved until 
the end of the chunk is founded")

(defun ontodot-subprocess-filter (process output)
  "Filter for the subprocess. It saves the data into a variable waiting for
 more chunks. Also, calls ONTODOT-FILTER-HOOK when a SWI prompt is found."
  ;; Insert chunk into process buffer
  (with-current-buffer (process-buffer ontodot-subprocess)
    (goto-char (point-max))
    (insert output)
    )
  ;; Add the chunk into the output variable
  (setq ontodot-subprocess-output
        (concat ontodot-subprocess-output output))
  ;; If the end is found, call the ONTODOT-FILTER-HOOK with the complete output
  ;; also, reset the saved output.
  (when (string-match "\\?-" output)
    (when ontodot-filter-hook
      (funcall ontodot-filter-hook ontodot-subprocess-output)
      )
    (setq ontodot-subprocess-output "")
    )  
  ) ;; defun


(defun ontodot-start-subprocess ()
  "Start the SWI subprocess if it is not already started."
  (when (not ontodot-subprocess)
    (setq ontodot-subprocess
          (start-process "ontodot-subprocess" "*ontodot-subprocess*" "swipl"))
    ;; Set the filter to catch when the output is ready
    (set-process-filter ontodot-subprocess 'ontodot-subprocess-filter)

    (ontodot-subprocess-send (concat
                              "use_module(library(ontodot)), "
                              "use_module(library(semweb/rdf_db)), "
                              "use_module(library(semweb/rdf_prefixes))"))
    ) ;; when
  ) ;; defun

(defun ontodot-subprocess-send (data)
  "Send DATA to the subprocess."
  (ontodot-start-subprocess)
  (with-current-buffer (process-buffer ontodot-subprocess)
    (goto-char (point-max))
    (insert (concat data "\n"))
    )
  (process-send-string ontodot-subprocess (concat data ", !. \n") )
  ) ;; defun


(defun ontodot-draw-node-graph (node-name)
  "Create a PNG image with the given node at its center. "
  (ontodot-subprocess-send
   (concat
    "dot_graph(" node-name ", '" ontodot-temp-png-file "')")
   )
  ) ;; defun

(defun ontodot--make-register-prefix-cmd (lst-prefixes)
  (mapconcat (lambda (element)
               (concat "rdf_register_prefix('"
                       (symbol-name (car element)) "', '"
                       (cdr element) "')")
               )
             lst-prefixes
             ", ")
  )

(defun ontodot-load-prefixes (lst-prefixes)
  "Load a prefix into the subprocess.
The LST-PREFIXES variable is an alist of (prefix . iri) elements."
  (ontodot-subprocess-send
   (ontodot--make-register-prefix-cmd lst-prefixes)
   )
  )

(defconst ontodot-prefix-regexp
  "@prefix[[:space:]]+\\([^:]*\\):[[:space:]]+<\\([^>]+\\)>[[:space:]]+."
  "The regexp used to recognize prefixs declarations.
First group matches the prefix, second group matches the IRI.")

(defun ontodot-search-prefixes (&optional buffer)
  "Search for prefixes in the given BUFFER. If BUFFER is nil, then use
the current buffer."
  (unless buffer
    (setq buffer (current-buffer))
    )

  (let ((lst-prefixes '())
        )
    (save-excursion
      (goto-char (point-min))
      (while (search-forward-regexp ontodot-prefix-regexp nil t)
        (add-to-list 'lst-prefixes
                     (cons (make-symbol (match-string-no-properties 1))
                           (match-string-no-properties 2))
                     t)
        )
      ) ;; save-excursion

    lst-prefixes
    ) ;; let
  ) ;; defun


(defun ontodot--show-node-callback (output)
  "Show the image generated by `ontodot-draw-node-graph'."
  (if (file-exists-p ontodot-temp-png-file)
      (find-file ontodot-temp-png-file)
    (message "Graph image couldn't be founded (ontodot has got problems).")
    )
  (setq ontodot-filter-hook nil)
  ) ;; defun


                                        ; Interactive functions

(defun ontodot-load-file (&optional filename)
  "Load the current file into the subprocess "
  (interactive "f")
  (unless filename
    (setq filename (buffer-file-name (current-buffer)))
    )
  (ontodot-subprocess-send
   (concat "load_ttl('" filename "')")
   )
  ) ;; defun

(defun ontodot-load-current-file ()
  "Load the current file."
  (interactive)
  (ontodot-load-file)
  ) ;; defun

(defun ontodot-show-node (node-name)
  "Show a node graph."
  (interactive "sNode name (prefix:name or IRI)?")
  (setq ontodot-filter-hook 'ontodot--show-node-callback)
  (ontodot-draw-node-graph node-name)  
  ) ;; defun

(defun ontodot-current-node ()
  "Return the current prefix:suffix founded near the cursor."
  (save-excursion
    (search-backward-regexp "\\(^\\|[[:space:]]\\)")
    (search-forward-regexp "\\([^: ]*\\):\\([^[:space:]]+\\)")
    (string-trim (match-string-no-properties 0))
    )
  ) ;; defun


(defun ontodot-show-current-node ()
  "Show the node that references the current cursor."
  (interactive)
  (ontodot-show-node (ontodot-current-node))
  ) ;; defun


(defun ontodot-load-current-prefixes ()
  "Load the prefixes declared in the current turtle document."
  (interactive)
  (ontodot-load-prefixes (ontodot-search-prefixes))
  (message "Prefixes on the current document loaded.")
  ) ;; defun

(defun ontodot-quit-subprocess ()
  "Halt the subprocess."
  (interactive)
  (ontodot-subprocess-send "halt")
  (setq ontodot-subprocess nil)
  (message "Subprocess halted.")
  ) ;; defun


;;; ontodot.el ends here



